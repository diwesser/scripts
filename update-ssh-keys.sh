#! /bin/bash

# Automatically download ssh keys from gitlab. Works well with cron.

# Set SSH directory path
ssh_dir="$HOME/.ssh"

# Download current keys from github
wget https://gitlab.com/diwesser.keys -O $ssh_dir/authorized_keys-downloaded

# Overwrite authorised keys with new file if it's not empty
if [ -s $ssh_dir/authorized_keys-downloaded ]
then
    # Tack a new line on to be safe
    printf "\n" >> authorized_keys-downloaded
    # Append local keys to downloaded keys
    cat $ssh_dir/authorized_keys-local >> $ssh_dir/authorized_keys-downloaded
    # Overwrite local keys with remote keys
    mv $ssh_dir/authorized_keys-downloaded $ssh_dir/authorized_keys
else
# Delete it if it is empty
    rm $ssh_dir/authorized_keys-downloaded
fi
