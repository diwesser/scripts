#/bin/bash
# Script to go through a directory to find duplicate archived youtube videos

default_IFS="$IFS"
IFS=$'\n'      # Change Internal Field Seperator to newline char

delete-kruft () {
    find ./ -type f | grep -E "^./20[0-9]{6} .*\.nfo|description|swp$" | xargs -d"\n" rm
}

# List videos matching video ID
# Check if there are multiple video files (mkv, mp4, webm)
videos-found () {
    find ./ -type f | grep --fixed-strings "$1" | grep --perl-regex ".*\.(?:mkv|mp4|webm)$"
}

# Cound videos found
video-count () {
    videos-found "$@" | wc -l
}

# Video file name to base file name
# Takes file path, outputs filename minus extension
get-file-basename () {
    i="$@"
    i="${i##*/}"
    i="${i%.*}"
    printf "$i"
}

# Get video file width.
# Takes video file path, outputs width in pixels
get-width () {
    exiftool -s3 -ImageWidth $@
}

# Get video dimensions
# Takes video file path, outputs [width]x[height]
get-video-dimensions () {
    exiftool -s3 -ImageSize $@
}

# Takes list of videos, outputs largest resolution
find-widest-video () {
    maxWidth=0
    for i in $@
    do
        res=$(get-width "$i")
        if [[ $res -gt $maxWidth ]]; then
            maxWidth=$res
        fi
    done
    printf $maxWidth
}

main () {

    # Delete superflous files
    delete-kruft

    # Generate a sorted list of unique video IDs
    uniq_ids=( $(find ./ -type f | grep -o -E "\[youtube [a-zA-Z0-9\-\_\-]{11}\]" | sort -u) )
    echo "Created array with ${#uniq_ids[@]} video IDs."

    index=$((${#uniq_ids[@]}-1))
    until [ $index -lt 0 ]
    do
        echo ""
        videoID=${uniq_ids[$index]}
        echo "Checking for duplicates of $videoID."
        echo "Found $(video-count $videoID videos.) video file(s)."
        listOfVidFiles=( $(videos-found $videoID) )

        # If duplicates found, first delete low resolution
        if [[ "$(video-count $videoID)" -gt 1 ]]; then
            widestRes=$(find-widest-video "${listOfVidFiles[@]}")
            echo "Widest video found: $widestRes"

            # Delete low resolution
            for i in "${listOfVidFiles[@]}"
            do
                echo "Checking resoluton of: $i"
                echo "Resolution is $(get-width $i)"

                if [ "$(get-width $i)" -lt $widestRes ]; then
                    echo "Deleting ./$(get-file-basename $i)."*
                    rm "./$(get-file-basename $i)."*
                fi
            done
        fi

        # If duplicates still found, delete by codec/container type
        if [[ "$(video-count $videoID)" -gt 1 ]]; then
            listOfVidFiles=( $(videos-found $videoID) )   # Update list of video files
            if [[ "${listOfVidFiles[@]}" =~ ".mkv"$ ]]; then
                echo "MKV found"
                for i in "${listOfVidFiles[@]}"
                do
                    if [[ "${i,,}" != *".mkv" ]]; then
                        echo "Deleting $i"
                        rm -- "$i"
                    fi
                done
            elif [[ "${listOfVidFiles[@]}" =~ ".webm"$ ]]; then
                echo "webm found"
                for i in "${listOfVidFiles[@]}"
                do
                    if [[ "${i,,}" != *".webm" ]]; then
                        echo "Deleting $i"
                        rm -- "$i"
                    fi
                done
            elif [[ "${listOfVidFiles[@]}" =~ ".mp4"$ ]]; then
                echo "MP4 found"
                for i in "${listOfVidFiles[@]}"
                do
                    if [[ "${i,,}" != *".mp4" ]]; then
                        echo "Deleting $i"
                        rm -- "$i"
                    fi
                done
            fi
        fi

        # If duplicates still found, delete files without resolution in title
        #if [[ "$(video-count $videoID)" -gt 1 ]]; then
        #    listOfVidFiles=( $(videos-found $videoID) )   # Update list of video files

        #    # Delete by file name
        #    for i in "${listOfVidFiles[@]}"
        #    do
        #    
        #    done
        #fi
            # Delete by corosponding metadata

        index=$((index-1))
    done
}

main
