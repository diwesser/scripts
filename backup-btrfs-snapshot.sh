#!/bin/bash

# Notes:
# - This program only recognises one snapshot root pattern
#
# Example use
# ./backup-btrfs-snapshot.sh --src="-e ssh user@server:/source/dir" \
#                            --dest="/backup/path" --snap-dir="snapshots"

# Todo:
# [ ] Find some way to enable this script to deal with privileged files without
#     being run as sudo.
#     - Option 1:
#       Try using sudo with a locked down config. This could mean
#       A) allowing this script to be run with elevated permissions on the target
#          path or
#       B) allowing Rsync to be run on the target path and calling Rsync with
#          sudo in this script.
#     - Option 2:
#       Rsync's --fake-super option. This would enable to preserve super user
#       permissions without having to give the script superuser permissions. If
#       you do this, you'll need a restore mode.
# [ ] Trim trailing slashes from given paths
# [ ] Do not create snapshot when running Rsync with --dry-run
# [ ] Add flag to specify SSH port


# Takes two arguments: "$dest" then "$snapshot"
create_snapshot () {
    $commandOutputSwitch btrfs subvolume snapshot -r "$1" "$2"
}

# Takes to arguments: "$src" then "$dest"
backup () {
    $commandOutputSwitch $sudoSwitch rsync \
        --recursive \
        --links         `#copy symlinks as links` \
        --hard-links    `#preserve hard links` \
        --specials      `#preserve special files` \
        --times         `#preserve time` \
        --group         `#preserve groups` \
        --owner         `#preserve owner - REQUIRES SUPERUSER` \
        --perms         `#preserve permissions` \
        --acls          `#preserve ACLs` \
        --xattrs        `#preserve extended attributes`\
        --delete-during `#delete extranious files on destination` \
        --exclude="/$snapRoot" \
        "$checksumSwitch" \
        "$outputSwitch" \
        "$testRunSwitch" \
        -- \
        "$1/" \
        "$2/"
}

print_help () {
    local a="-f Date to backup. (From)\n"
    a="$a-t Backup target path. (To)\n"
    a="$a-s Snapshot root name.\n"
    a="$a-q Suppress output. (Quiet)\n"
    a="$a-d Dry run.\n"
    a="$a-S Run Rsync with Sudo\n"
    a="$a-c Print generated commands to terminal and exit."
    a="$a\n"
    a="$a\n"
    a="$a-h Print help text (this output)\n"

    printf -- "$a"
}

main () {
    backupDate="$(date +'%Y-%m-%d %H%M%S %Z' --utc)"
    src=""
    dest=""
    snapRoot="snapshots"
    outputSwitch="--progress --verbose"
    testRunSwitch=""
    sudoSwitch=""

    while getopts "f:t:s:p:qdSh" opt; do
        case "$opt" in
            f) src="$OPTARG" ;;
            t) dest="$OPTARG" ;;
            s) snapRoot="$OPTARG" ;;
            q) outputSwitch="--quiet" ;;
            d) testRunSwitch="--dry-run" ;;
            S) sudoSwitch="sudo" ;;
            c) commandOutputSwitch="echo"
               printf "Unfortunately output removes quotes from file paths:\n" ;;
            h) print_help
                exit 0 ;;
            \?) printf "Invalid option. (-h for help)\n" >&2
                exit 1 ;;
        esac
    done

    snapshot="$dest/$snapRoot/$backupDate"

    printf -- "The commands to run are:\n"
    create_snapshot "$dest" "$snapshot"
    printf "\n"
    backup "$src" "$dest"
}

main "$@"
