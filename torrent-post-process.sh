#!/bin/bash

# This script creates a hard link clone of a directory of downloaded files then
# moves the files to a seperate directory to deal with. The intended use case
# for this script is to seed file and also easily rename them.
#
# Todo:
# [ ] Find a way to handle duplicate files. Maybe something with `cp --update`
# [ ] Maybe switch to using options instead of three arguments in order
# [ ] If a path is not given, root is used. Stop that.
# [ ] This will not copy empty directories into the seed folder. Fix that.

downloadDir="$1"
seedDir="$2"
postDir="$3"

readarray -d '' files < <(find "$downloadDir" -mindepth 1 -print0)

# Seed Directory
for from in "${files[@]}"
do
    # Create new paths
    link="${from/$downloadDir/$seedDir}"

    # Create hardlinks in seed dir (and parents as needed)
    mkdir -p -- "${link%/*}"

    # Create hardlinks in seed dir (and parents as needed)
    cp -l -- "$from" "$link"
done

# Post Process Directory
for from in "${files[@]}"
do
    # Create post process path
    post="${from/$downloadDir/$postDir}"

    # Move linked files from download to post process dir
    mv -- "$from" "$post"
done
