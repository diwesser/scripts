#!/bin/bash
#
# This script shows prints the battery percentage and an icon that indicates
# whether or not the computer is plugged into power.
#
# This script is designed for use in a status bar.
#
# Todo:
# [ ] Switch to extracting three key bits of info as variables. Bits are:
#     - battery percentage
#     - charging state (fully charged, charging, discharging), and 
#     - time to charge/time remaining
#  [ ] Select which bits of info to print with option flags.
#      (Or maybe just make a couple small related scripts up? Thse will be
#      called frequently and that would likely be more efficient.)

percentage=$(cat /sys/class/power_supply/BAT0/uevent | grep POWER_SUPPLY_CAPACITY= | cut -d'=' -f2)
powerSource="!"

if [[ -f /sys/class/power_supply/BAT0/uevent ]] ; then
    if [[ $(cat /sys/class/power_supply/BAT0/uevent | grep POWER_SUPPLY_STATUS |
          cut -d'=' -f2) == "Discharging" ]] ; then
        powerSource="⚪ "
    elif [[ $(cat /sys/class/power_supply/BAT0/uevent | grep POWER_SUPPLY_STATUS |
            cut -d'=' -f2) == "Charging" ]] ; then
        powerSource="⚇ "
    elif [[ $(cat /sys/class/power_supply/BAT0/uevent | grep POWER_SUPPLY_STATUS |
            cut -d'=' -f2) == "Unknown" ]] ; then
        powerSource="⚇ "
    fi
    #echo $percentage% $powerSource
    echo $powerSource$percentage%
else
    powerSource="⚇ "
    echo "$powerSource"
fi

