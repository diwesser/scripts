#!/bin/bash

# Script for creating and streaming a randomly made up disposable playlist
# from youtube.
#
# Todo:
#   - Actually test this version
#   - If no flags given, treat as -p/--play

playlist="$HOME/.config/diwesser/otfplaylist"

# -n|--next
# Add song to top of playlist
add_top () {
    echo "$(echo "$1" | cat - $playlist)" > $playlist
}

# -a|--add
# Add song to end of playlist
add_bottom () {
    echo "$1" >> $playlist
}

# -p|--play
# Play song
play () {
    # While the playlist isn't empty
    while [[ $(cat $playlist) ]] ; do
        # Get the first url
        nextSong="$(head -n 1 $playlist)"
        # Remove it from the playlist
        tail -n +2 "$playlist" > "$playlist.tmp" && mv "$playlist.tmp" "$playlist"
        # And play it
        mpv --vid=no $nextSong
    done
    echo "No songs in playlist"
}

# -c|--clear
# Clear playlist
clear_playlist () {
    rm $playlist
    touch $playlist
}

# -q|--queue
# Print the titles of the songs
print_cue () {
    while read nextInCue; do
        yt-dlp --get-title $nextInCue
    done < $playlist
}

# -h|-\?|--help
# Print help text
print_help () {
    echo "On The Fly PLAYlist is a script for making"
    echo "disposable Youtube audio playlists."
    echo ""
    echo " -a|--add <url>    # Add song to end of playlist"
    echo " -n|--next <url>   # Add as next song"
    echo " -p|--play         # Start playlist"
    echo " -q|--queue        # Print cued titles in order"
    echo " -c|--clear        # Clear playlist"
}

main () {
    echo "This version of otfplay is entirely untested."
    echo "Version one is strongly recommended."

    while :; do
        case $1 in
            -h|-\?|--help) print_help ;;
            -a|--add) add_bottom"$2";;
            -c|--clear) clear_playlist ;;
            -n|--next) add_top "$2";;
            -p|--play) play ;;
            -q|--queue) print_cue ;;
            --) ;;
            -?*) echo "Invalid option: $1" ;;
            *)
                break
        esac
        shift
    done
}

main "$@"
