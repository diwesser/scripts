#!/bin/bash

# Script to update DuckDNS entries. Works best as a cron job.

# Enter DuckDNS credentials here
subdomain=""
token=""

# Make log file path
logfile="$HOME/duckdns/logs/$(date --iso-8601=date --utc)"

# Make logfile dir if needed
mkdir -p "$HOME/duckdns/logs"

printf "$(date --iso-8601=seconds --utc):\n" >> "$logfile"
curl https://www.duckdns.org/update/$subdomain/$token >> "$logfile"
printf "\n" >> "$logfile"
