#!/bin/bash
#
# Calculate hole spacing and list of drill points
#
# Todo:
# - Take variables as arguments
# - Optimise. Maybe see if calculations can be done with pure bash/posix? The 
#   Print out has visible processing time.
# - Round numbers instead of just trimming. There's some good examples here:
#   https://askubuntu.com/questions/179898/
# - Text formatting in printspecs function.
# - Help text

offset=0
holecount=24
furthestholeseperation=18
decimalpoints=3 # Enough to show 1/8 of an inch
holespacing=$(printf "$furthestholeseperation/$holecount\n" | bc -ql)

function printspecs () {
    printf "Number of holes: $holecount\n"
    printf "Distance between futhest holes: $furthestholeseperation\n"
    printf "Offset to first hole: $offset\n"
    printf "Hole spacing: "
    printf %."$decimalpoints"f "$holespacing"
    printf "\n"
}

printspecs
printf "\n"

i=$((holecount-1))

function distancetohole () {
    printf %."$decimalpoints"f \
        "$(printf "($furthestholeseperation/($holecount-1)*$i+$offset)\n" | bc -ql)"
    printf "\n"
    }

while [[ $i -gt 0 ]] ; do
    distancetohole
    i=$((i-1))
done

distancetohole
